from django.contrib import admin
from django.urls import path, include

# DataFlair adi
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('libraryapp.urls')),
]